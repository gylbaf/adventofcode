class Sprite {
    #position = 1;

    get position() { return this.#position; }
    set position(value) { this.#position = value; } 
    intersects(value){ return value >= this.#position -1 && value <= this.#position + 1; }
}