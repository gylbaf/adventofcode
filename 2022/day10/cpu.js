class Cpu {
    #instructions = [];
    #currentInstruction = null;
    #registerX = 1;
    #cycle = 1;

    constructor(instructions){
        this.#instructions = instructions;
    }

    runNextCycle(){
        if (this.#currentInstruction == null){
            let instruction = this.#instructions.shift();
            //console.log(instruction);
            this.#currentInstruction = new Instruction(instruction);
            //console.log(this.#currentInstruction);
        }
        this.#currentInstruction.run();
        if (this.#currentInstruction.isFinished()){
            this.#registerX += this.#currentInstruction.value;
            this.#currentInstruction = null;
        }
        this.#cycle++;
        //console.log(this.#cycle + " " + this.#registerX);
    }
    get registerX() { return this.#registerX; }
    get cycle() { return this.#cycle; }
    get instructions() { return this.#instructions; }
}

const CpuCommand = {
    Noop: 0,
    Addx: 1,
    Undefined: 2
}

class Instruction {
    #command;
    #value = 0;
    #cyclesLeft = 1;
    constructor(instruction){
        if (instruction.split(' ')[0] == 'noop'){
            this.#command = CpuCommand.Noop;
        }
        else if (instruction.split(' ')[0] == 'addx'){
            this.#command = CpuCommand.Addx;
            this.#value = +instruction.split(' ')[1];
            this.#cyclesLeft = 2;
        }
        else{
            this.#command = CpuCommand.Undefined;
        }
    }
    run(){
        this.#cyclesLeft--;
    }
    isFinished() { return this.#cyclesLeft <= 0; }
    get value() { return this.#value; }
}