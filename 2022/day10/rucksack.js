class Rucksack {
    #compartment1 = null;
    #compartment2 = null;

    constructor(value){
        this.#compartment1 = new Compartment(value.substring(0, value.length/2));
        this.#compartment2 = new Compartment(value.substring(value.length/2));
    }

    get equalItems() {
        let result = [];
        for(let item of this.#compartment1.items){
            result = result.concat(this.#compartment2.items.filter(i=>i.name==item.name));
        }
        return result;
    }
    containsItemValue(value){
        return this.getItems().filter(item=>item.name==value).length > 0;
    }

    get equalItemsScore() {
        return this.equalItems.reduce((sum, item)=>{
            return sum + item.value;
        }, 0);
    }

    getItems(){
        return this.#compartment1.items.concat(this.#compartment2.items);
    }
}

class Compartment {
    #items = [];
    constructor(value){
        for(let item of value){
            if (this.exists(item)) continue;
            this.#items.push(new Item(item));
        }
    }
    get items() { return this.#items; }
    exists(value){ return this.#items.filter(item=>item.name==value).length > 0; }
}

class Item {
    #value;
    constructor(value){
        this.#value = value;
    }
    get name() { return this.#value; }
    get value() {
        if (this.charValue < 97) return this.charValue - 65 + 27;
        return this.charValue - 96; 
    }

    get charValue(){ return this.#value.charCodeAt(0); }
}