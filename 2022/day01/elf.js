class Elf {
    #calories = [];

    AddCalories(value){
        if (value){
            this.#calories.push(+value);
        }
    }

    get totalCalories() {
        let result = 0;
        this.#calories.forEach(value => {
            result += value;
        });
        return result;
    }
}