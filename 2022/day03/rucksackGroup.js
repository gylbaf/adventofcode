class RucksackGroup {
    #rucksacks = [];

    add(value) { this.#rucksacks.push(value); }
    get rucksacks() { return this.#rucksacks; }
    getEqualItemValue(){
        for(let item of this.#rucksacks[0].getItems()){
            if(this.#rucksacks[1].containsItemValue(item.name) &&
            this.#rucksacks[2].containsItemValue(item.name)){
                return item.value;
            }
        }
        return 0;
    }
}