class Gesture {
    #name = "";
    #score = 0;
    #canBeat = "";

    constructor(name, score, canBeat) {
        this.#name = name;
        this.#score = score;
        this.#canBeat = canBeat;
    }

    get score() { return this.#score; }
    get name() { return this.#name; }

    canBeat(opponent) {
        return this.#canBeat == opponent.name;
    }

    static create(value) {
        if (value == "A" || value == "X") {
            return new Gesture("Rock", 1, "Scissors");
        }
        if (value == "B" || value == "Y") {
            return new Gesture("Paper", 2, "Rock");
        }
        if (value == "C" || value == "Z") {
            return new Gesture("Scissors", 3, "Paper");
        }
    }

    static createStrategically(strategy, opponent) {
        if (strategy == "X") {//loose
            switch (opponent.name) {
                case "Rock":
                    return Gesture.create("C");
                case "Paper":
                    return Gesture.create("A");
                case "Scissors":
                    return Gesture.create("B");
            }
        }
        if (strategy == "Y") {//draw
            switch (opponent.name) {
                case "Rock":
                    return Gesture.create("A");
                case "Paper":
                    return Gesture.create("B");
                case "Scissors":
                    return Gesture.create("C");
            }
        }
        if (strategy == "Z") {//win
            switch (opponent.name) {
                case "Rock":
                    return Gesture.create("B");
                case "Paper":
                    return Gesture.create("C");
                case "Scissors":
                    return Gesture.create("A");
            }
        }
    }
}