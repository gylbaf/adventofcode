class Elevator {
    #level = 0;
    #numOfInstructions = 0;

    AddInstruction(value){
        if (value == "(") this.#level++;
        if (value == ")") this.#level--;
        this.#numOfInstructions++;
    }

    get level() { return this.#level; }
    get numOfInstructions() { return this.#numOfInstructions; }
}