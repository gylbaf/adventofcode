<?php
$indata = file_get_contents("indata1.txt");
$indata = explode("\n", $indata);
$points = new Points([]);
foreach($indata as $row){
    $split = explode(",", $row);
    $points->points[] = new Point($split[0], $split[1]);
}
$points->CalcMinMax();
$points->Draw();
$points->getAreaChars();
print_r(count($points->getPointLess10000()));

class Point{
    public function __construct($x, $y){
        $this->x = (int)trim($x);
        $this->y = (int)trim($y);
    }

    public function getDist($other){
        return abs($this->x-$other->x) + abs($this->y-$other->y);
    }
}
class Points{
    public $minX = 99999;
    public $minY = 99999;
    public $maxX = 0;
    public $maxY = 0;
    
    public function __construct($points){
        $this->points = $points;
    }
    public function CalcMinMax(){        
        foreach($this->points as $point){
            $this->minX = $point->x < $this->minX ? $point->x : $this->minX;
            $this->minY = $point->y < $this->minY ? $point->y : $this->minY;
            $this->maxX = $point->x > $this->maxX ? $point->x : $this->maxX;
            $this->maxY = $point->y > $this->maxY ? $point->y : $this->maxY;
        }
    }
    public function getDrawString(){
        $str = [];
        for($y = 0; $y <= $this->maxY; $y++){
            $row = '';
            for($x = 0; $x <= $this->maxX; $x++){
                $np = $this->GetNearestPoint(new Point($x, $y));
                $row .= $np < 0 ? '.' : chr(65+$np);
            }
            $str[] = $row;
        }
        foreach($this->points as $i=>$point){
            $str[$point->y][$point->x] = chr($i+65);
        }
        return $str;
    }
    public function GetNearestPoint($p){
        $minDist = 999999;
        $dublett = false;
        $minIndex = -1;
        foreach($this->points as $i=>$point){
            $dist = $point->getDist($p);
            if ($dist == $minDist) $dublett = true;
            if ($dist < $minDist){
                $dublett = false;
                $minDist = $dist;
                $minIndex = $i;
            }
        }
        if ($dublett) return -1;
        return $minIndex;
    }
    public function getPointLess10000(){
        $points = [];
        for($y = 0; $y <= $this->maxY; $y++){
            for($x = 0; $x <= $this->maxX; $x++){
                $dist = 0;
                $p = new Point($x, $y);
                foreach($this->points as $i=>$point){
                    $dist += $point->getDist($p);
                }
                if ($dist < 10000)
                    $points[] = $point;
            }
        }
        return $points;
    }
    public function getRandPoints(){
        $randIndexes = '';
        $str = $this->getDrawString();
        for($i=0; $i<count($str); $i++){
            if ($i == 0 || $i==count($str) - 1){
                $randIndexes .= $str[$i];
            }
            else{
                $randIndexes .= $str[$i][0];
                $randIndexes .= $str[$i][strlen($str[$i])-1];
            }
        }
        return $randIndexes;
    }
    public function Draw(){
        echo "<code>";
        foreach($this->getDrawString() as $row){
            echo "$row<br>";
        }
        echo "</code>";
    }
    public function getAreaChars(){
        $ds = $this->getDrawString();
        $ds = implode("", $ds);
        $rp = $this->getRandPoints();
        for($i=0; $i<strlen($rp);$i++){
            $ds = str_replace($rp[$i], "", $ds);
        }
        for($i=65; $i<65+100; $i++){
            echo chr($i)." - ".substr_count($ds, chr($i))."<br>";
        }
        print_r($ds);
    }
}