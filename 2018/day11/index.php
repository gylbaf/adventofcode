<?php
$day11 = new Day11(8199);
//$day11->plot();

echo $day11->MaxPowerCell();
class Day11{
    public function __construct($snr){
        $this->serialNumber = $snr;
    }
    public $serialNumber = 8;

    public function GetPowerLevel($x, $y){
        $rackId = $x + 10;
        $powerLevel = $rackId * $y;
        $powerLevel += $this->serialNumber;
        $powerLevel *= $rackId;
        $powerLevel = floor($powerLevel%1000/100);
        $powerLevel -= 5;
        return $powerLevel;
    }

    public function GetPowerCell($x, $y, $size){
        if ($x+$size > 301 || $y+$size > 301) return -100;
        $result = 0;
        for($i=0; $i<$size; $i++){
            for($j=0; $j<$size; $j++){
                $result += $this->GetPowerLevel($x+$j, $y+$i); 
            }
        }
        return $result;
    }

    public function plot(){
        for($y=1; $y<=300; $y++){
            for($x=1; $x<=300; $x++){
                echo $this->GetPowerLevel($x, $y)." ";
            }
            echo "<br>";
        }
    }
    public function MaxPowerCell(){
        $max = 0;
        $maxStr = "";
        $size = 1;
        for($y=1; $y<=300; $y++){
            for($x=1; $x<=300; $x++){
                $size = 10;
                while(true){
                    $v = $this->GetPowerCell($x, $y, $size);
                    if ($v > $max){
                        $max = $v;
                        $maxStr = "($x, $y, $size) => $max";
                    }
                    $size++;
                    if ($v == -100 || $size > 50) break;
                }
            }
        }
        return $maxStr;
    }
}

/*
The rack ID is 3 + 10 = 13.
The power level starts at 13 * 5 = 65.
Adding the serial number produces 65 + 8 = 73.
Multiplying by the rack ID produces 73 * 13 = 949.
The hundreds digit of 949 is 9.
Subtracting 5 produces 9 - 5 = 4.
*/ 