<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);
//2 3 0 3 10 11 12 1 1 0 1 99 2 1 1 2
$indata = file_get_contents("indata1.txt");
$indata = explode(" ", $indata);
$data = [];
$rot = [];
foreach($indata as $item){
    $data[] = (int)$item;
}
$children = [];
$index = 0;
$char = 'A';
$rot['children'] = [];
getChild($data, $children);
$checksum = 0;
foreach($children as $child){
    foreach($child->metadata as $metadata){
        $checksum += $metadata;
    }
}
//print_r($children[0]);die;
echo $children[0]->getValue()."<br>";
echo $checksum;
function getChild(&$data, &$children){
    $child = new Node();
    $child->numOfChildren = $data[0];
    $child->numOfMetadata= $data[1];
    $children[] = $child;
    unset($data[0]);
    unset($data[1]);
    $data = array_values($data);
    for($i=0; $i<$child->numOfChildren;$i++){
        $child->children[] = getChild($data, $children);
    }
    for($i=0; $i<$child->numOfMetadata;$i++){
        $child->metadata[] = $data[$i];
        unset($data[$i]);
    }
    $data = array_values($data);
    return $child;
}
class Node{
    public $name = "A";
    public $numOfChildren = 0;
    public $numOfMetadata = 0;
    public $children = [];
    public $metadata = [];
    public function getValue(){
        //print_r($this->children);
        if (count($this->children)==0){
            //echo "A";
            $value = 0;
            foreach($this->metadata as $metadata){
                $value += $metadata;
            }
            return $value;
        }
        else{
            //echo "B";
            $value = 0;
            foreach($this->metadata as $metadata){
                $value += !empty($this->children[$metadata-1]) ?$this->children[$metadata-1]->getValue() : 0;
            }
            return $value; 
        }
    }
}