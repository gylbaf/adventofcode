<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);
//2 3 0 3 10 11 12 1 1 0 1 99 2 1 1 2
$numOfPlayer = 425;
$lastMarble = 7084800;
$playerScore = [];
for($i=0; $i<$numOfPlayer;$i++){
    $playerScore[$i] = 0;
}
$player = 0;
$game = new Game();
for($i=1;$i<=$lastMarble;$i++){
    $player = $i%$numOfPlayer;
    if ($i%23 == 0){
        $game->Rotate(-7);
         $score = $game->data->Pop();
         $game->Rotate(1);
         $playerScore[$player] += $i + $score;
    }
    else{
        $game->Rotate(1);
        $game->Add($i);
    }
    //$game->print();
}
//print_r($playerScore);
$max = 0;
foreach($playerScore as $score){
    if ($score > $max) $max = $score;
}
echo $max;
class Game{
    public $data = null;
    public function __construct(){
        $this->data = new LList();
        $this->data->Add(0);
    }
    public function Add($value){
        $this->data->Add($value);
    }
    public function Rotate($value){
        $this->data->Rotate($value);
    }
    public function print(){
        $i = $this->data->rot;
        while(true){
            //echo "Value: ".$i->value." Prev: ".$i->prev->value." Next:".$i->next->value."<br>";
            if ($i->value == $this->data->current->value) echo "<b>";
                echo $i->value." ";
            if ($i->value == $this->data->current->value) echo "</b>";
            $i = $i->next;
            if ($i->value == $this->data->rot->value) break; 
        }
        echo "<br>";
    }
}
class LList{
    public $rot = null;
    public $current = null;
    public function Next(){

    }
    public function Value(){
        return $this->current->value;
    }
    public function rotate($num){
        for($i=0;$i<abs($num); $i++){
            if ($num > 0)
                $this->current = $this->current->next;
            else $this->current = $this->current->prev;
        }
    }
    public function Pop(){
        $return = $this->Value();
        $this->remove();
        return $return;
    }
    public function Add($value){
        $node = new Node($value);
        if ($this->rot == null){
            $this->current = $node;
            $this->current->next = $node;
            $this->current->prev = $node;
            $this->rot = $this->current;
        }
        else{
            $node->next = $this->current->next;
            $node->prev = $this->current;
            $this->current->next->prev = $node;
            $this->current->next = $node;
            $this->current = $node;
        }

    }
    public function remove(){
        $this->current->next->prev = $this->current->prev;
        $this->current->prev->next = $this->current->next;
        $this->current = $this->current->prev;
    }
}

class Node{
    public $next = null;
    public $prev = null;
    public $value = "";

    public function __construct($value){
        $this->value = $value;
    }
}