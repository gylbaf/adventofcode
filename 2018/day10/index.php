<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);
$indata = file_get_contents("indata1.txt");
$points = [];
foreach(explode("\n", $indata) as $str){
    $points[] = getPointFromString($str);
}

$min = getWidth($points);
$ticks = 0;
while(true){
    $newWidth = getWidth($points);
    if ($newWidth > $min) break;
    Tick($points);
    $min = $newWidth;
    $ticks++;
}
echo $ticks."<br>";
UnTick($points);
Minimize($points);
function getWidth($points){
    return getMaxX($points) - getMinX($points);
}
function getMinX($points){
    $min = $points[0]->x;
    foreach($points as $point){
        if ($point->x < $min) $min = $point->x;
    }
    return $min;
}
function getMinY($points){
    $min = $points[0]->y;
    foreach($points as $point){
        if ($point->y < $min) $min = $point->y;
    }
    return $min;
}
function getMaxX($points){
    $max = $points[0]->x;
    foreach($points as $point){
        if ($point->x > $max) $max = $point->x;
    }
    return $max;
}
function getMaxY($points){
    $max = $points[0]->y;
    foreach($points as $point){
        if ($point->y > $max) $max = $point->y;
    }
    return $max;
}
function Tick(&$points){
    foreach($points as &$point){
        $point->x += $point->dx;
        $point->y += $point->dy;
    }
}
function UnTick(&$points){
    foreach($points as &$point){
        $point->x -= $point->dx;
        $point->y -= $point->dy;
    }
}
function Minimize(&$points){
    $minX = getMinX($points);
    $minY = getMinY($points);
    foreach($points as &$point){
        $point->x -= $minX;
        $point->y -= $minY;
    }
}
function getPointFromString($str){
    $pos11 = strpos($str, "position=<");
    $pos12 = strpos($str, ">", $pos11);
    $pos21 = strpos($str, "velocity=<");
    $pos22 = strpos($str, ">", $pos21);
    $s1 = substr($str, $pos11+10, $pos12-$pos11-10);
    $s2 = substr($str, $pos21+10, $pos22-$pos21-10);
    $data1 = explode(",",$s1);
    $data2 = explode(",",$s2);
    return new Point((int)$data1[0], (int)$data1[1], (int)$data2[0], (int)$data2[1]);
}
class Point{
    public function __construct($x, $y, $dx, $dy){
        $this->x = $x;
        $this->y = $y;
        $this->dx = $dx;
        $this->dy = $dy;
    }
}

$lines = [];
for($y=0; $y <= getMaxY($points); $y++){
    for($x=0; $x <= getMaxX($points); $x++){
        $lines[$y][$x] = ".";
    }
}
foreach($points as $point){
    $lines[$point->y][$point->x] = "#";
}
echo "<code>";
for($y=0; $y <= getMaxY($points); $y++){
    for($x=0; $x <= getMaxX($points); $x++){
        echo $lines[$y][$x];
    }
    echo "<br>";
}
echo "</code>";
?>
<a href="?tid=<?php echo ($tid+1);?>">Next</a>
