import os
clear = lambda: os.system('cls')

class Cart:
    x = 0
    y = 0
    dx = 0
    dy = 0
    state = 0
    def __init__(self, x, y, ch):
        self.x = x
        self.y = y
        if (ch == '>'): self.setRight()
        if (ch == '<'): self.setLeft()
        if (ch == '^'): self.setUp()
        if (ch == 'v'): self.setDown()
    def __str__(self):
        return '({}, {}, {}, {})'.format(self.x, self.y, self.dx, self.dy);
    def move(self):
        self.x += self.dx
        self.y += self.dy
    def turnLeft(self):
        if (self.dx == 1):
            self.setUp()
        elif (self.dx == -1):
            self.setDown()
        elif (self.dy == -1):
            self.setLeft()
        else:
            self.setRight()
    def turnRight(self):
        if (self.dx == 1):
            self.setDown()
        elif (self.dx == -1):
            self.setUp()
        elif (self.dy == -1):
            self.setRight()
        else:
            self.setLeft()
    def setDirection(self, dirChar):
        if (dirChar == "\\"): 
            if (self.dx == 1): 
                self.setDown()
            elif (self.dx == -1): 
                self.setUp()
            elif (self.dy == -1): 
                self.setLeft()
            else: self.setRight()
        if (dirChar == "/"): 
            if (self.dy == 1): 
                self.setLeft()
            elif (self.dx == -1): 
                self.setDown()
            elif (self.dx == 1): 
                self.setUp()
            else: 
                self.setRight()
        if (dirChar == "+"): 
            if (self.state == 0): 
                self.turnLeft()
                self.state = 1
            elif (self.state == 1): 
                self.state = 2
            elif (self.state == 2): 
                self.turnRight()
                self.state = 0
        
        """self.dx = 0
        self.dy = 0        
        if (ch == '>'): self.dx = 1"""
    def setLeft(self):
        self.dx = -1
        self.dy = 0        
    def setRight(self):
        self.dx = 1
        self.dy = 0        
    def setUp(self):
        self.dx = 0
        self.dy = -1        
    def setDown(self):
        self.dx = 0
        self.dy = 1        

with open("indata1.txt") as f:
    indata = f.read().splitlines()
def findCarts():
    global indata
    carts = []
    while True:
        y = 0
        ch = ''
        cart = None
        for row in indata:
            if ('>' in row): ch = '>'
            if ('<' in row): ch = '<'
            if ('^' in row): ch = '^'
            if ('v' in row): ch = 'v'
            if (ch != ''): 
                cart = Cart(row.find(ch), y, ch)
                rowList = list(row)
                replaceChar = '|'
                if cart.y == 0 : replaceChar = '-'
                rowList[cart.x] = replaceChar
                indata[y] = ''.join(rowList)
                break
            y += 1
        if (cart == None): break
        carts.append(cart)
    return carts
carts = findCarts()
def printCarts(carts):
    global indata
    for iy, row in enumerate(indata):
        for ix, ch in enumerate(row):
            printCart = False
            for cart in carts:
                if (iy==cart.y and ix==cart.x): printCart = True
            if (printCart): print('#', end='')
            else: print(ch, end='')
        print()
def getMapChar(cart):
    global indata
    for iy, row in enumerate(indata):
        for ix, ch in enumerate(row):
            if (iy==cart.y and ix==cart.x): return ch
    return ''
def checkCollision():
    global carts
    for i, cart1 in enumerate(carts):
        for j, cart2 in enumerate(carts):
            if (i==j): continue
            if cart1.x == cart2.x and cart1.y == cart2.y:
                print("Collision")
                print(cart1)
                print(cart2)
                carts.remove(cart1)
                carts.remove(cart2)
                print(len(carts))
                return
def printCartsStat():
    global carts
    for cart in carts:
        print(cart)

clear()
print(carts[0])
print(carts[1])
printCarts(carts)
input("Press Enter to continue...")
while(True):
    #clear()
    #print(carts[0])
    #print(carts[1])
    for cart in carts:
        cart.move()
        cart.setDirection(getMapChar(cart))
    #printCarts(carts)
    checkCollision()
    if (len(carts) == 1): 
        for cart in carts:
            cart.move()
        break
    #input("Press Enter to continue...")
clear()
printCartsStat()
input("Press Enter to continue...")
