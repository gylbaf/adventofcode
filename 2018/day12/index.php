<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);
//2 3 0 3 10 11 12 1 1 0 1 99 2 1 1 2
$indata = file_get_contents("indata1.txt");
$indata = explode("\n", $indata);
$potsString = substr($indata[0], 15);
$pots = new Pots();
$pots->addPots($potsString);

for($i=2; $i<count($indata);$i++){
    $pots->addRule(new Rule($indata[$i]));
}
echo "<code>";
$pots->print();
for($i=0; $i<1000; $i++){
    echo ($i+1)."_".$pots->getFirstPlantIndex();
    $pots->CalcNextGeneration();
    //$pots->print();
    echo "Sum: ".$pots->GetSum()."<br>";
}
echo "</code>";
$value = 50000000000-110;
$startValue = 7370;
echo "<br>Sum: ".(7370 + $value*67);

class Pots{
    public $pots = [];
    public $rules = [];
    public function __construct(){
        for($i=-10;$i<300; $i++){
            $this->pots[$i] = ".";
        }
    }
    public function getFirstPlantIndex(){
        foreach($this->pots as $index=>$pot){
            if ($pot == "#") return $index;
        }
        return -1;
    }
    public function print(){
        foreach($this->pots as $index=>$pot){
            echo $pot;
        }
        echo "<br>";
    }
    public function GetSum(){
        $sum = 0;
        foreach($this->pots as $index=>$pot){
            $sum += $pot=="#" ? $index : 0;
        }
        return $sum;
    }
    public function CalcNextGeneration(){
        $this->pots = $this->getNextGeneration();
    }
    public function getNextGeneration(){
        $pots = [];
        foreach($this->pots as $index=>$pot){
            $rule = $this->GetMatchingRule($this->getPotString($index));
            $pots[$index] = !empty($rule)?($rule->result?"#":"."):".";
        }
        return $pots;
    }
    public function GetMatchingRule($string){
        foreach($this->rules as $rule){
            if ($rule->match == $string)
                return $rule;
        }
        return null;
    }
    public function getPotString($index){
        if ($index -2 < -10 || $index + 2 > 299) return "";
        return $this->pots[$index-2].$this->pots[$index-1].$this->pots[$index].$this->pots[$index+1].$this->pots[$index+2];
    }
    public function addRule($rule){
        $this->rules[] = $rule;
    }
    public function addPots($str){
        for($i=0;$i<strlen($str); $i++){
            $this->pots[$i] = $str[$i] == "#"?"#":".";
        }
    }
}
class Rule{
    public $match = "";
    public $result = false;
    public function __construct($str){
        //...## => #
        $this->match = substr($str, 0, 5);
        $this->result = substr($str, 9, 1) == "#";
    }
}